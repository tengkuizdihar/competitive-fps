# Installation

Everytime you're on your first run or changes the nix files, please run this command.

```shell
nix-build default.nix -A inputDerivation -o .nix-shell-inputs -vv && nix-shell default.nix
```

This will create a .nix-shell-inputs which will persist the dependency of the project between GC. It will also make sure that when you delete the project, your dependency will also be GC because .nix-shell-inputs is deleted.
Subsequent nix-shell command shouldn't do this build phase as long as the nix files isn't changed. More information could be found here https://web.archive.org/web/20220708201311/https://ianthehenry.com/posts/how-to-learn-nix/saving-your-shell/.

# Configuring Godot + VSCodium + NixOS

1. Configuring Godot
   1. Retrieve VSCodium absolute path using `whereis codium` and take notes of it.
   2. Open up editor settings from the Top Menu => Editor => Text Editor => External
   3. Check `Use External Editor`.
   4. Paste the absolute path of VSCodium into `Exec Path`.
   5. Fill `Exec Flags` with `{project} --goto {file}:{line}:{col}`.
2. Configuring VSCodium
   1. Retrieve Godot Engine absolute path using `whereis godot` and take notes of it.
   2. Download godot-tools from the extension manager.
   3. Open up the settings and fill `editor_path` with Godot Engine's absolute path.

Restart both of the editor and engines, then enjoy your newly made editor! Also, now you could start up the debugger and use the breakpoint using `F5`.